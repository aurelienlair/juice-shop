# Juice shop application on a Kubernetes cluster

The current project is a fork of the [Juice Shop](https://github.com/juice-shop/juice-shop), a [Node.js](https://nodejs.org/en) web application. The original intention was to utilize the application for conducting security tests in adherence to [OWASP](https://owasp.org) guidelines. However, in the current context, the objective is to deploy the application on a [Kubernetes](https://kubernetes.io) cluster within an ephemeral environment, while also conducting performance tests using [Locust](https://docs.locust.io/en/stable/what-is-locust.html). While the current project is largely based on the original one, it includes some minor modifications to align with the specific objectives of the current context. The original README can still be accessed [here](README.fork.md).

## System requirements

To get started, please follow these steps to install the latest versions of Node.js and Docker for your specific operating system:

Install the latest version of [Node.js](https://nodejs.org/en) for your OS. You can find the official Node.js website and download the installer suitable for your operating system. Follow the installation instructions provided by the Node.js installer to complete the setup.

Install the latest version of [Docker](https://www.docker.com/community-edition) for your OS. Visit the official Docker website and choose the Docker version compatible with your operating system. Download and run the Docker installer, following the installation instructions specific to your OS.

By installing the latest versions of Node.js and Docker, you'll have the necessary tools to proceed with your project.

## Running Juice Shop application

### In a local environment without docker

```bash
cd PATH_TO_JUICE_SHOP_REPOSITORY
npm install # only has to be done before first start or when you change the source code
npm run start

> juice-shop@14.5.1 start
> node build/app

info: Server listening on port 3000
```

Browse to [http://localhost:3000](http://localhost:3000)

### In a local environment with docker

To build and push the Docker image of the application to the GitLab registry follow these instructions:

Start by logging in

```bash
docker login --username $GITLAB_USER --password $GITLAB_PERSONAL_ACCESS_TOKEN registry.gitlab.com
```

Then build and push

```bash
docker build \
    --no-cache \
    --progress=plain \
    --tag "registry.gitlab.com/aurelienlair/juice-shop:latest" \
    .

docker push registry.gitlab.com/aurelienlair/juice-shop:latest
```

You can double check locally by running the docker image in order to check if it's working properly

```bash
docker run --publish 3000:3000 registry.gitlab.com/aurelienlair/juice-shop:latest

info: All dependencies in ./package.json are satisfied (OK)
info: Chatbot training data botDefaultTrainingData.json validated (OK)
info: Detected Node.js version v18.15.0 (OK)
info: Detected OS linux (OK)
info: Detected CPU x64 (OK)
info: Configuration default validated (OK)
info: Entity models 19 of 19 are initialized (OK)
info: Required file server.js is present (OK)
info: Required file index.html is present (OK)
info: Required file styles.css is present (OK)
info: Required file main.js is present (OK)
info: Required file tutorial.js is present (OK)
info: Required file polyfills.js is present (OK)
info: Required file runtime.js is present (OK)
info: Required file vendor.js is present (OK)
info: Port 3000 is available (OK)
info: Server listening on port 3000
```

Browse to [http://localhost:3000](http://localhost:3000)

## In a Kubernetes cluster with ephemeral environments in Hetzer cloud

Take a look at [this](https://gitlab.com/aurelienlair/kubernetes-orchestrator) repository for instructions on running Juice Shop on a Kubernetes cluster.

## Running tests against Juice Shop application

In this project, pushing a branch* initiates the creation of a Kubernetes cluster with ephemeral environments in the Hetzner Cloud. Once the application is operational, it automatically launches performance tests on the deployed Juice Shop application.

*Branches with the name pattern `build.*` exclusively trigger the execution of the `build` job

### Configuration of the pipeline trigger token

To begin with, it's crucial to ensure proper configuration of an API token and SSH keys in Hetzner, as detailed in [this](https://gitlab.com/aurelienlair/kubernetes-orchestrator#authentication) README. At this point you can configure it in the Juice Shop project CI/CD settings together with a [pipeline trigger token](https://docs.gitlab.com/ee/ci/triggers/#create-a-pipeline-trigger-token) as shown:

<img src="./docs/images/cicd_variables.png" alt="cicd_variables" style="width:80%;"/>

With the pipeline trigger token setup, we can initiate another pipeline within a project named [kubernetes-orchestrator](https://gitlab.com/aurelienlair/kubernetes-orchestrator). This pipeline will then have the capability to generate a Kubernetes cluster with ephemeral environments in the Hetzner Cloud, incorporating the Juice Shop application Docker image from our branch.

### Running non-functional tests

#### Running security tests

At this stage, after the cluster is created and our application is deployed, a third pipeline will be generated within a project named [performance-tests](https://gitlab.com/aurelienlair/security-tests). This pipeline will execute security tests using [Zap](https://en.wikipedia.org/wiki/ZAP_(software)) on our ephemeral Juice Shop application and produce a report using GitLab artifacts.

To view the report, follow this sequence:

<img src="./docs/images/" alt="run_security_tests" style="width:80%;"/>
<img src="./docs/images/" alt="browse_security_tests_artifacts" style="width:80%;"/>
<img src="./docs/images/" alt="security_reports_html" style="width:80%;"/>

At this point you can see Zap report:

<img src="./docs/images/zap_report.png" alt="zap_report" style="width:80%;"/>

Following the completion of the entire security test suite, the ephemeral environment will be eliminated from the Hetzner Cloud through a final pipeline triggered in [kubernetes-orchestrator](https://gitlab.com/aurelienlair/kubernetes-orchestrator).

#### Running performance tests

At this stage, after the cluster is created and our application is deployed, a third pipeline will be generated within a project named [performance-tests](https://gitlab.com/aurelienlair/performance-tests). This pipeline will execute performance tests on our ephemeral Juice Shop application and produce a [Locust](https://locust.io/) report using GitLab artifacts.

To view the report, follow this sequence:

<img src="./docs/images/run_performance_tests.png" alt="run_performance_tests" style="width:80%;"/>
<img src="./docs/images/browse_performance_artifacts.png" alt="browse_peformance_artifacts" style="width:80%;"/>
<img src="./docs/images/performance_reports_html.png" alt="performance_reports_html" style="width:80%;"/>

At this point you can see Locust report:

<img src="./docs/images/locust_report.png" alt="locust_report" style="width:80%;"/>

Following the completion of the entire performance test suite, the ephemeral environment will be eliminated from the Hetzner Cloud through a final pipeline triggered in [kubernetes-orchestrator](https://gitlab.com/aurelienlair/kubernetes-orchestrator).
## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of a run command
```

| Type         | Description                                                                                            |
| ------------ | ------------------------------------------------------------------------------------------------------ |
| `style`    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build`    | Changes to the build process                                                                           |
| `chore`    | Changes to the build process or auxiliary tools and libraries such as documentation generation         |
| `docs`     | Documentation updates                                                                                  |
| `feat`     | New features                                                                                           |
| `fix`      | Bug fixes                                                                                              |
| `refactor` | Code refactoring                                                                                       |
| `test`     | Adding missing tests                                                                                   |
| `perf`     | A code change that improves performance                                                                |
